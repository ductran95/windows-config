# Windows 10 installation for Lenovo Legion 5

# BIOS
* AMD CBS => NBIO >= SMU => CPPC => Enabled

# Disk partitions
* Disk 1: 931GB
  * Windows: 200GB
  * Works: 131GB
  * Games: 600GB
* Disk 2: 476GB
  * Arch-linux: 126GB
  * Entertainment: 350GB

# Create Installation USB
*  en-us_windows_10_enterprise_ltsc_2021_x64_dvd_d289cf96.iso
*  Rufus: GPT-UEFI-NTFS

# Install Windows
*  F1 + F2 => USB
*  Language: English, Format: English, Keyboard: US
*  Skip enter key
*  Custom: Install windows only
*  Delete partitions
*  Username: DucTran
*  Online speech: Off, Diagnosis: Off, Tailored: Off

# Config UAC
*  Open Control Panel
*  View: Large Icons
*  User Accounts => Change User Accounts Control Setting
    *  Never Notify

# Config Power Option
*  Open Control Panel => Power Options
*  Choose what closing the lid
    *  power: sleep
    *  sleep: sleep
    *  close lid: Do nothing
*  Balaced => Change plan setting
    *  Turn off: 30 min
    *  Sleep: never
*  Change advanced setting
    *  Turn off hard disk: 0
    *  Wireless Adapter Setting: Medium power save, Maximun performance
    *  USB setting: Disabled
    *  PCI Express: Off
    *  Processor power => Minimum process state: 5%
    *  Processor power => System colling: Passive, Active
    *  Processor power => Maximum process state: 100%
    *  Multimedia => Sharing media: Allow sleep, Prevent sleep
    *  Multimedia => Video Playback: power-saving, performance
    *  Multimedia => Play video: balanced, video quality
    *  Battery => Critical battery notify: On
    *  Battery => Critical battery action: Shutdown
    *  Battery => Low battery level: 10%
    *  Battery => Critical battery level: 5%
    *  Battery => Low battery notify: On
    *  Battery => Low battery action: Nothing
    *  Battery => Reserved battery level: 7%

# Config File Explorer setting
*  Open File Explorer
*  File => Change folder and search options => View
    *  Show hidden file, folder and drive
    *  Untick Hide empty drive
    *  Untick Hide extension
    *  Untick Hide folder merge conflict

# Config Sync
*  Win+R: gpedit.msc
*  Computer Configuration => Admin Template => Windows Components => Sync your setting
*  Enabled all options
*  Computer Configuration => Admin Template => System => File system => Enable Win32 long path

# Config Service
*  Win+R: services.msc
*  Disable:
    *  Microsoft Software Shadow Copy
    *  Optimize drive
    *  Sysmain
    *  Volumn Shadow Copy

# Config Task Schedule
*  Win+R: taskschd.msc
*  Microsoft => Windows
*  Disable:
    *  Application Experience
    *  Application Data
    *  CloudExperienceHost
    *  Customer Experience Improvement Program
    *  Defrag
    *  DiskCleanup
    *  SystemRestore

# Turn off Hibernate file
*  Open cmd as admin
```bash
powercfg -h off
```

# Config Time & Region
*  Open Settings => Time & Language
*  Timezone: UTC +7
*  Open Control Panel => Region
*  Format: Vietnamese

# Reboot

# Delete Windows SR (optional)
*  Open Control Panel => System => Advanced system settings => System Protection
*  Configure => Delete

# Config Page file (optional)
*  Open Control Panel => System => Advanced system settings => Performance => Setting
*  Advanced => Virtual memery => Change
    *  Automatical manage: untick
    *  C: Turn off
*  Set => OK
*  Restart

# Update windows
*  Open Settings => Update & Security
*  Advanced Options => Receive updates for other Microsoft product: On
*  Delivery Optimization: Off
*  Check for update
*  Restart

# Active Window
*  Setting => Update => Activation => Product Key: QPM6N-7J2WJ-P88HH-P3YRH-YY74H

# Install MS Store
*  Open cmd as admin
```bash
wsreset -i
```

# Install Drivers
*  Update MS Store
*  MS Store => Lenovo vantage, lenovo hotkey
*  Update driver
*  AMD 
*  Geforce
*  Fix AMD startup bug: Device Manager => System => AMD Audio CoProcessor => update driver
*  Restart

# Disable Windows Update driver
*  wumt: hide all drivess
*  Restart

# Install TLS1.2
*  TLS1.reg

# Customize
*  Customize Setting
    *  Display
    *  System Sound
    *  System Notification
    *  Input
    *  Personalization
    *  Privacy

# Update Power option
*  powercfg.exe -attributes sub_processor perfboostmode -attrib_hide
*  powercfg.exe -attributes sub_processor unattendsleep -attrib_hide
*  Open Control Panel => Power Options
*  Balaced => Change plan setting
*  Change advanced setting
    *  Processor performance boost mode: On battery: Disabled
    *  Processor performance boost mode: Plugged in: Effiency Enabled
	*  Sleep: System unattended sleep time out: On battery: 0
	*  Sleep: System unattended sleep time out: Plugged in: 0

# Rename PC

# Install Office 2021
*  en_office_professional_plus_2019_x86_x64_dvd_7ea28c99 => setup64
*  Active
*  Update
*  Restart

# Install SQL Server 2019 (optional)
*  en_sql_server_2019_enterprise_x64_dvd_5e1ecc6b.iso
*  Setting SQL service => Manual
*  Restart

# Install IIS
*  Turn on feature
*  Add IIS
*  Remove IE11

# Install Visual Studio
*  vs_Enterprise.exe
*  Restart

# Install Winget
*  MS Store => App Installer

# Setting Winget
*  winget settings --enable InstallerHashOverride

# Install Winget UI
*  winget install --id SomePythonThings.WingetUIStore

# Install Terminal
*  winget install --id Microsoft.WindowsTerminal --interactive --skip-dependencies
*  winget install --id Microsoft.PowerShell
*  winget install --id JanDeDobbeleer.OhMyPosh

# Install SDK & Development Environment
* winget install --id Notepad++.Notepad++
* winget install --id Microsoft.VisualStudioCode
* winget install --id Microsoft.AzureDataStudio
* winget install --id GnuPG.Gpg4win
* winget install --id Git.Git --interactive
* winget install --id Microsoft.DotNet.SDK.6
* winget install --id Microsoft.DotNet.SDK.8
* winget install --id GoLang.Go
* winget install --id Oracle.JDK.21
* winget install --id Schniz.fnm
* fnm use --install-if-missing 22
* fnm use --install-if-missing 20
* fnm use --install-if-missing 18
* winget install python3
* winget install --id JesseDuffield.lazygit
* winget install --id Codeblocks.Codeblocks
* winget install --id Atlassian.Sourcetree
* winget install --id Postman.Postman
* winget install --id Microsoft.SQLServerManagementStudio
* winget install --id MongoDB.Compass.Full
* winget install --id MongoDB.DatabaseTools
* winget install --id DBBrowserForSQLite.DBBrowserForSQLite
* winget install --id k6.k6
* winget install --id JetBrains.Rider --interactive
* winget install --id JetBrains.WebStorm --interactive
* winget install --id JetBrains.DataGrip --interactive
* winget install --id JetBrains.GoLand --interactive
* Run [MinGW-W64 online installer](https://github.com/Vuniverse0/mingwInstaller/releases/download/1.2.1/mingwInstaller.exe) as Admin install to C:\Program Files
* add C:\Program Files\mingw64\bin to PATH

# .NET extensions
* Config Nuget source
```bash
%appdata%\NuGet\NuGet.Config

<?xml version="1.0" encoding="utf-8"?>
<configuration>
	<packageSources>
		<add key="NuGet official package source" value="https://api.nuget.org/v3/index.json" />
	</packageSources>
</configuration>
```
```bash
dotnet tool install --global dotnet-ef
dotnet tool install --global dnt
dotnet new install Ardalis.CleanArchitecture.Template
dotnet new install Clean.Architecture.Solution.Template
dotnet new install IdentityServer4.Templates
dotnet new install Duende.IdentityServer.Templates
dotnet new install Avalonia.Templates
dotnet new install HotChocolate.Templates
dotnet new install MassTransit.Templates
```

# Git config
```bash
git config --global user.name "Duc Tran"
git config --global user.email tv.duc95@gmail.com
git config --global --add safe.directory *
git config --global core.longpaths true
git config --global gpg.program "C:\Program Files (x86)\GnuPG\bin\gpg.exe"
gpg --import "D:\Certificates\PGP\Tran Van Duc_0x7AAECCA2_SECRET.asc"
```

# Install Desktop util
* winget install --id Rainmeter.Rainmeter
* Install rainformer_hwinfo.rmskin
* Add widget
  * Clock
  * CPU
  * GPU
  * Disk
  * Network

# Install Document util
* mobireadersetup.msi
* creator.msi
* winget install --id calibre.calibre
* winget install --id SumatraPDF

# Install Internet Browser
* winget install --id Google.Chrome
* winget install --id Ablaze.Floorp --interactive
* winget install --id VivaldiTechnologies.Vivaldi --interactive
* winget install --id Brave.Brave --interactive

# Install Download Manager
* winget install --id qBittorrent.qBittorrent
* [File Centipede](https://github.com/filecxx/FileCentipede/raw/main/release/filecxx_latest_win_x64.zip)
* [Fix FileCentipede](https://voz.vn/t/tong-hop-software-can-thiet-cho-may-tinh.2974/page-64#post-21105427)

# Install Media Player
* winget install --id PeterPawlowski.foobar2000 -a x86
* DarkOne310build20140207.exe 
* copy ProgramComponent => Program File (x86)/foobar2000/components
* copy foobar2000 => Appdata/Roaming
* winget install --id CodecGuide.K-LiteCodecPack.Mega --interactive

# Install Media Editor
* dMC-Ref-Registered.exe
* winget install --id Nikse.SubtitleEdit

# Install System util
* [Sysinternals Suite](https://download.sysinternals.com/files/SysinternalsSuite.zip)
* Extract SysinternalsSuite.zip + Add to path

# Install Other util
* winget install --id 7zip.7zip
* winget install --id TGRMNSoftware.BulkRenameUtility --ignore-security-hash (open with normal, not admin)
* winget install --id namazso.OpenHashTab
* winget install --id REALiX.HWiNFO
* winget install --id MHNexus.HxD
* winget install --id Resplendence.LatencyMon
* winget install --id MediaArea.MediaInfo.GUI
* winget install --id Rufus.Rufus
* winget install --id AlexanderKojevnikov.Spek
* winget install --id Stellarium.Stellarium
* winget install --id RARLab.WinRAR
* winget install --id Valve.Steam
* winget install --id EpicGames.EpicGamesLauncher
* winget install --id WireGuard.WireGuard
* winget install --id Skillbrains.Lightshot
* winget install --id Jellyfin.JellyfinMediaPlayer
* winget install --id Bitwarden.Bitwarden
* Download [Softether VPN Client](https://www.softether-download.com/en.aspx?product=softether)
* Download [Samsung Magician](https://semiconductor.samsung.com/consumer-storage/support/tools/)
* Download [Legion Fan Control](https://www.legionfancontrol.com)

# Install Disk util
* MiniTool_Partition_Wizard_12.8

# Fonts
* [Noto Sans](https://fonts.google.com/noto/specimen/Noto+Sans)
* [Noto Serif](https://fonts.google.com/noto/specimen/Noto+Serif)
* [Cascadia Code](https://github.com/microsoft/cascadia-code/releases/download/v2111.01/CascadiaCode-2111.01.zip)

# Restart

# Login and sync OneDrive

# Install MS Store app
* Skype
* Photo
* Calculator
* Mail
* To Do
* Sticky Note
* Filelight
* KDE Connect
* Telegram
* Termius
* Am lich

# Customize Taskbar

# Customize Language

# Set Default App

# Delete temp file

# Add ICC profile

# Customize Windows Terminal
* Install Meslo font
```bash
oh-my-posh font install
```
* Create Powershell Profile
```bash
if (!(Test-Path -Path $PROFILE )) { New-Item -Type File -Path $PROFILE -Force }
```
* Update Profile
```bash
oh-my-posh init pwsh --config "$env:POSH_THEMES_PATH\quick-term.omp.json" | Invoke-Expression
```
* Update Terminal font: CTRL + SHIFT + ,
```bash
{
    "profiles":
    {
        "defaults":
        {
            "font":
            {
                "face": "MesloLGM Nerd Font"
            }
        }
    }
}
```

# Other (optional)
* Universal x86 Tuning Utility https://github.com/JamesCJ60/Universal-x86-Tuning-Utility/releases
* Filezilla Pro

# Rclone (optinal)
```bash
rclone rcd --rc-web-gui
rclone --vfs-cache-mode writes --vfs-cache-max-size 20G mount gdrive: X: --network-mode
```