# Install Arch Linux on WSL
* [ArchWSL](https://github.com/yuk7/ArchWSL).
* [Setting up Arch Linux with KDE Plasma in Windows Subsystem for Linux 2](https://rashil2000.me/blogs/kde-arch-wsl).
  
# Enable WSL
*  Control Panel => Program => Turn on Features
*  Enable HyperV & WSL

# Install Arch WSL
* Download appx & cer
* Install cer to Local Machine => Trusted People
* Install appx

# Config Arch user
* Open cmd: Arch.exe
* passwd
* echo "%wheel ALL=(ALL) ALL" > /etc/sudoers.d/wheel
* useradd -m -G wheel -s /bin/bash ductran
* passwd ductran
* exit
* Open cmd: Arch.exe config --default-user ductran

# Config Arch pacman
* Open cmd: Arch.exe
* sudo pacman-key --init
* sudo pacman-key --populate

# Install KDE
* sudo pacman -Syyyu
* sudo pacman -S base base-devel nano
* sudo pacman -S plasma

# Config X Server
* Install [GWSL](https://opticos.github.io/gwsl/)